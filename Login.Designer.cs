﻿namespace SamplePDA
{
    partial class Login
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.btnCreateUser = new System.Windows.Forms.Button();
            this.lblVer = new System.Windows.Forms.Label();
            this.btn_Close = new System.Windows.Forms.Button();
            this.btn_Login = new System.Windows.Forms.Button();
            this.txt_Password = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_UserCD = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCreateUser
            // 
            this.btnCreateUser.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.btnCreateUser.Location = new System.Drawing.Point(171, 249);
            this.btnCreateUser.Name = "btnCreateUser";
            this.btnCreateUser.Size = new System.Drawing.Size(67, 18);
            this.btnCreateUser.TabIndex = 38;
            this.btnCreateUser.Text = "계정생성";
            this.btnCreateUser.Click += new System.EventHandler(this.btnCreateUser_Click);
            // 
            // lblVer
            // 
            this.lblVer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lblVer.Location = new System.Drawing.Point(2, 248);
            this.lblVer.Name = "lblVer";
            this.lblVer.Size = new System.Drawing.Size(237, 20);
            this.lblVer.Text = "Ver 1.0.0";
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(133, 192);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(76, 27);
            this.btn_Close.TabIndex = 35;
            this.btn_Close.Text = "닫기";
            // 
            // btn_Login
            // 
            this.btn_Login.Location = new System.Drawing.Point(29, 192);
            this.btn_Login.Name = "btn_Login";
            this.btn_Login.Size = new System.Drawing.Size(76, 27);
            this.btn_Login.TabIndex = 34;
            this.btn_Login.Text = "로그인";
            // 
            // txt_Password
            // 
            this.txt_Password.Location = new System.Drawing.Point(92, 118);
            this.txt_Password.Name = "txt_Password";
            this.txt_Password.PasswordChar = '*';
            this.txt_Password.Size = new System.Drawing.Size(120, 21);
            this.txt_Password.TabIndex = 33;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(16, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 20);
            this.label3.Text = "패스워드";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txt_UserCD
            // 
            this.txt_UserCD.Location = new System.Drawing.Point(92, 93);
            this.txt_UserCD.Name = "txt_UserCD";
            this.txt_UserCD.Size = new System.Drawing.Size(120, 21);
            this.txt_UserCD.TabIndex = 32;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(16, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 20);
            this.label2.Text = "사용자ID";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 270);
            this.Controls.Add(this.btnCreateUser);
            this.Controls.Add(this.lblVer);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.btn_Login);
            this.Controls.Add(this.txt_Password);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txt_UserCD);
            this.Controls.Add(this.label2);
            this.Menu = this.mainMenu1;
            this.Name = "Login";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCreateUser;
        private System.Windows.Forms.Label lblVer;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.Button btn_Login;
        private System.Windows.Forms.TextBox txt_Password;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_UserCD;
        private System.Windows.Forms.Label label2;
    }
}

